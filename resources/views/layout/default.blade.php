<!doctype html>
<html lang="en" class="no-ie">
<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="description" content="">
   <meta name="keywords" content="">
   <meta name="author" content="">
   <title>@yield('title','Home') - Sistem Informasi Siswa</title>
   @include('include.css')
   @stack('css')
</head>
<body>
   <div class="wrapper">
      @include('include.header')
      @include('include.sidebar')
      @yield('content')
   </div>
   @include('include.js')
   @stack('js')
</body>
</html>
