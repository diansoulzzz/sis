<script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- Plugins-->
<script src="{{asset('assets/vendor/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/slider/js/bootstrap-slider.js')}}"></script>
<script src="{{asset('assets/vendor/filestyle/bootstrap-filestyle.min.js')}}"></script>
<!-- Animo-->
<script src="{{asset('assets/vendor/animo/animo.min.js')}}"></script>
<!-- Sparklines-->
<script src="{{asset('assets/vendor/sparklines/jquery.sparkline.min.js')}}"></script>
<!-- Slimscroll-->
<script src="{{asset('assets/vendor/slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- Store + JSON-->
<script src="{{asset('assets/vendor/store/store%2bjson2.min.js')}}"></script>
<!-- Classyloader-->
<script src="{{asset('assets/vendor/classyloader/js/jquery.classyloader.min.js')}}"></script>
<!-- START Page Custom Script-->
<!--  Flot Charts-->
<script src="{{asset('assets/vendor/flot/jquery.flot.min.js')}}"></script>
<script src="{{asset('assets/vendor/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{asset('assets/vendor/flot/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('assets/vendor/flot/jquery.flot.pie.min.js')}}"></script>
<script src="{{asset('assets/vendor/flot/jquery.flot.time.min.js')}}"></script>
<script src="{{asset('assets/vendor/flot/jquery.flot.categories.min.js')}}"></script>
<!--[if lt IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
<!-- END Page Custom Script-->
<!-- App Main-->
<script src="{{asset('assets/js/app.js')}}"></script>
