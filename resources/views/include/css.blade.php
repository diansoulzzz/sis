<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]><script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
<!-- Bootstrap CSS-->
<link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
<!-- Vendor CSS-->
<link rel="stylesheet" href="{{asset('assets/vendor/fontawesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/animo/animate%2banimo.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/csspinner/csspinner.min.css')}}">
<!-- START Page Custom CSS-->
<!-- END Page Custom CSS-->
<!-- App CSS-->
<link rel="stylesheet" href="{{asset('assets/css/app.css')}}">
<!-- Modernizr JS Script-->
<script src="{{asset('assets/vendor/modernizr/modernizr.js')}}" type="application/javascript"></script>
<!-- FastClick for mobiles-->
<script src="{{asset('assets/vendor/fastclick/fastclick.js')}}" type="application/javascript"></script>
<!-- <script src="ga.js" type="application/javascript"></script> -->
